<?php

namespace App\Entity;

use App\Repository\InteretRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InteretRepository::class)
 */
class Interet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameInteret;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionInteret;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="interets")
     */
    private $idUser;

    public function __construct()
    {
        $this->idUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameInteret(): ?string
    {
        return $this->nameInteret;
    }

    public function setNameInteret(string $nameInteret): self
    {
        $this->nameInteret = $nameInteret;

        return $this;
    }

    public function getDescriptionInteret(): ?string
    {
        return $this->descriptionInteret;
    }

    public function setDescriptionInteret(?string $descriptionInteret): self
    {
        $this->descriptionInteret = $descriptionInteret;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getIdUser(): Collection
    {
        return $this->idUser;
    }

    public function addIdUser(User $idUser): self
    {
        if (!$this->idUser->contains($idUser)) {
            $this->idUser[] = $idUser;
        }

        return $this;
    }

    public function removeIdUser(User $idUser): self
    {
        $this->idUser->removeElement($idUser);

        return $this;
    }
}
