<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titlejob;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $summary;

    /**
     * @ORM\ManyToMany(targetEntity=Tool::class, mappedBy="idUser")
     */
    private $tools;

    /**
     * @ORM\ManyToMany(targetEntity=Formation::class, mappedBy="idUser")
     */
    private $formations;

    /**
     * @ORM\ManyToMany(targetEntity=Interet::class, mappedBy="idUser")
     */
    private $interets;

    /**
     * @ORM\ManyToMany(targetEntity=Project::class, inversedBy="users")
     */
    private $idProject;

    public function __construct()
    {
        $this->tools = new ArrayCollection();
        $this->formations = new ArrayCollection();
        $this->interets = new ArrayCollection();
        $this->idProject = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getTitlejob(): ?string
    {
        return $this->titlejob;
    }

    public function setTitlejob(string $titlejob): self
    {
        $this->titlejob = $titlejob;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return Collection|Tool[]
     */
    public function getTools(): Collection
    {
        return $this->tools;
    }

    public function addTool(Tool $tool): self
    {
        if (!$this->tools->contains($tool)) {
            $this->tools[] = $tool;
            $tool->addIdUser($this);
        }

        return $this;
    }

    public function removeTool(Tool $tool): self
    {
        if ($this->tools->removeElement($tool)) {
            $tool->removeIdUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->addIdUser($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            $formation->removeIdUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Interet[]
     */
    public function getInterets(): Collection
    {
        return $this->interets;
    }

    public function addInteret(Interet $interet): self
    {
        if (!$this->interets->contains($interet)) {
            $this->interets[] = $interet;
            $interet->addIdUser($this);
        }

        return $this;
    }

    public function removeInteret(Interet $interet): self
    {
        if ($this->interets->removeElement($interet)) {
            $interet->removeIdUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getIdProject(): Collection
    {
        return $this->idProject;
    }

    public function addIdProject(Project $idProject): self
    {
        if (!$this->idProject->contains($idProject)) {
            $this->idProject[] = $idProject;
        }

        return $this;
    }

    public function removeIdProject(Project $idProject): self
    {
        $this->idProject->removeElement($idProject);

        return $this;
    }
}
