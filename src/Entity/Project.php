<?php

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameProject;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionProject;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $dateCreationProject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkGitProejct;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkSiteProject;

    /**
     * @ORM\ManyToMany(targetEntity=Tool::class, inversedBy="projects")
     */
    private $idTool;

    /**
     * @ORM\OneToMany(targetEntity=Images::class, mappedBy="idProject")
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="idProject")
     */
    private $users;

    public function __construct()
    {
        $this->idTool = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameProject(): ?string
    {
        return $this->nameProject;
    }

    public function setNameProject(string $nameProject): self
    {
        $this->nameProject = $nameProject;

        return $this;
    }

    public function getDescriptionProject(): ?string
    {
        return $this->descriptionProject;
    }

    public function setDescriptionProject(?string $descriptionProject): self
    {
        $this->descriptionProject = $descriptionProject;

        return $this;
    }

    public function getDateCreationProject(): ?string
    {
        return $this->dateCreationProject;
    }

    public function setDateCreationProject(?string $dateCreationProject): self
    {
        $this->dateCreationProject = $dateCreationProject;

        return $this;
    }

    public function getLinkGitProejct(): ?string
    {
        return $this->linkGitProejct;
    }

    public function setLinkGitProejct(?string $linkGitProejct): self
    {
        $this->linkGitProejct = $linkGitProejct;

        return $this;
    }

    public function getLinkSiteProject(): ?string
    {
        return $this->linkSiteProject;
    }

    public function setLinkSiteProject(?string $linkSiteProject): self
    {
        $this->linkSiteProject = $linkSiteProject;

        return $this;
    }

    /**
     * @return Collection|Tool[]
     */
    public function getIdTool(): Collection
    {
        return $this->idTool;
    }

    public function addIdTool(Tool $idTool): self
    {
        if (!$this->idTool->contains($idTool)) {
            $this->idTool[] = $idTool;
        }

        return $this;
    }

    public function removeIdTool(Tool $idTool): self
    {
        $this->idTool->removeElement($idTool);

        return $this;
    }

    /**
     * @return Collection|Images[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Images $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setIdProject($this);
        }

        return $this;
    }

    public function removeImage(Images $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getIdProject() === $this) {
                $image->setIdProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addIdProject($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeIdProject($this);
        }

        return $this;
    }
}
