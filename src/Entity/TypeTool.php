<?php

namespace App\Entity;

use App\Repository\TypeToolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeToolRepository::class)
 */
class TypeTool
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameTypeTool;

    /**
     * @ORM\OneToMany(targetEntity=Tool::class, mappedBy="idTypeTool")
     */
    private $tools;

    public function __construct()
    {
        $this->tools = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameTypeTool(): ?string
    {
        return $this->nameTypeTool;
    }

    public function setNameTypeTool(string $nameTypeTool): self
    {
        $this->nameTypeTool = $nameTypeTool;

        return $this;
    }

    /**
     * @return Collection|Tool[]
     */
    public function getTools(): Collection
    {
        return $this->tools;
    }

    public function addTool(Tool $tool): self
    {
        if (!$this->tools->contains($tool)) {
            $this->tools[] = $tool;
            $tool->setIdTypeTool($this);
        }

        return $this;
    }

    public function removeTool(Tool $tool): self
    {
        if ($this->tools->removeElement($tool)) {
            // set the owning side to null (unless already changed)
            if ($tool->getIdTypeTool() === $this) {
                $tool->setIdTypeTool(null);
            }
        }

        return $this;
    }
}
