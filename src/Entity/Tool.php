<?php

namespace App\Entity;

use App\Repository\ToolRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ToolRepository::class)
 */
class Tool
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameTool;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkImageTool;

    /**
     * @ORM\ManyToOne(targetEntity=TypeTool::class, inversedBy="tools")
     */
    private $idTypeTool;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="tools")
     */
    private $idUser;

    /**
     * @ORM\ManyToMany(targetEntity=Project::class, mappedBy="idTool")
     */
    private $projects;

    public function __construct()
    {
        $this->idUser = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameTool(): ?string
    {
        return $this->nameTool;
    }

    public function setNameTool(string $nameTool): self
    {
        $this->nameTool = $nameTool;

        return $this;
    }

    public function getLinkImageTool(): ?string
    {
        return $this->linkImageTool;
    }

    public function setLinkImageTool(?string $linkImageTool): self
    {
        $this->linkImageTool = $linkImageTool;

        return $this;
    }

    public function getIdTypeTool(): ?TypeTool
    {
        return $this->idTypeTool;
    }

    public function setIdTypeTool(?TypeTool $idTypeTool): self
    {
        $this->idTypeTool = $idTypeTool;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getIdUser(): Collection
    {
        return $this->idUser;
    }

    public function addIdUser(User $idUser): self
    {
        if (!$this->idUser->contains($idUser)) {
            $this->idUser[] = $idUser;
        }

        return $this;
    }

    public function removeIdUser(User $idUser): self
    {
        $this->idUser->removeElement($idUser);

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->addIdTool($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->removeElement($project)) {
            $project->removeIdTool($this);
        }

        return $this;
    }
}
