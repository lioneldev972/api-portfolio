<?php

namespace App\Entity;

use App\Repository\FormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 */
class Formation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameFormation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etablissementFormation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptifFormation;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $startFormation;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $endFormation;

    /**
     * @ORM\ManyToOne(targetEntity=TypeFormation::class, inversedBy="formations")
     */
    private $idTypeFormation;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="formations")
     */
    private $idUser;

    public function __construct()
    {
        $this->idUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameFormation(): ?string
    {
        return $this->nameFormation;
    }

    public function setNameFormation(string $nameFormation): self
    {
        $this->nameFormation = $nameFormation;

        return $this;
    }

    public function getEtablissementFormation(): ?string
    {
        return $this->etablissementFormation;
    }

    public function setEtablissementFormation(string $etablissementFormation): self
    {
        $this->etablissementFormation = $etablissementFormation;

        return $this;
    }

    public function getDescriptifFormation(): ?string
    {
        return $this->descriptifFormation;
    }

    public function setDescriptifFormation(?string $descriptifFormation): self
    {
        $this->descriptifFormation = $descriptifFormation;

        return $this;
    }

    public function getStartFormation(): ?string
    {
        return $this->startFormation;
    }

    public function setStartFormation(string $startFormation): self
    {
        $this->startFormation = $startFormation;

        return $this;
    }

    public function getEndFormation(): ?string
    {
        return $this->endFormation;
    }

    public function setEndFormation(?string $endFormation): self
    {
        $this->endFormation = $endFormation;

        return $this;
    }

    public function getIdTypeFormation(): ?TypeFormation
    {
        return $this->idTypeFormation;
    }

    public function setIdTypeFormation(?TypeFormation $idTypeFormation): self
    {
        $this->idTypeFormation = $idTypeFormation;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getIdUser(): Collection
    {
        return $this->idUser;
    }

    public function addIdUser(User $idUser): self
    {
        if (!$this->idUser->contains($idUser)) {
            $this->idUser[] = $idUser;
        }

        return $this;
    }

    public function removeIdUser(User $idUser): self
    {
        $this->idUser->removeElement($idUser);

        return $this;
    }
}
