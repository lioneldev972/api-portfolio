<?php

namespace App\Entity;

use App\Repository\TypeFormationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeFormationRepository::class)
 */
class TypeFormation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameTypeFormation;

    /**
     * @ORM\OneToMany(targetEntity=Formation::class, mappedBy="idTypeFormation")
     */
    private $formations;

    public function __construct()
    {
        $this->formations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameTypeFormation(): ?string
    {
        return $this->nameTypeFormation;
    }

    public function setNameTypeFormation(string $nameTypeFormation): self
    {
        $this->nameTypeFormation = $nameTypeFormation;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
            $formation->setIdTypeFormation($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->formations->removeElement($formation)) {
            // set the owning side to null (unless already changed)
            if ($formation->getIdTypeFormation() === $this) {
                $formation->setIdTypeFormation(null);
            }
        }

        return $this;
    }
}
