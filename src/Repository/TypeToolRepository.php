<?php

namespace App\Repository;

use App\Entity\TypeTool;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypeTool|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeTool|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeTool[]    findAll()
 * @method TypeTool[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeToolRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeTool::class);
    }

    // /**
    //  * @return TypeTool[] Returns an array of TypeTool objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeTool
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
